#!/bin/bash

mkdir /app/data
mkdir /app/data/wallets

BGLd -datadir=/app/data -walletdir=/app/data/wallets -conf=/app/BGL.conf -rpcallowip=0.0.0.0/0

/bin/bash "$@"
