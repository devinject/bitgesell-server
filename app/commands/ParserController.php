<?php


namespace app\commands;


use app\models\Address;
use app\models\Transaction;
use app\models\Vout;
use app\services\DataService;
use app\services\RpcService;
use app\services\ParserService;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

class ParserController extends Controller
{
    /**
     * @var DataService
     */
    private $addressesService;
    /**
     * @var RpcService
     */
    private $rpcService;
    /**
     * @var ParserService
     */
    private $transactionsService;

    public function __construct($id, $module, DataService $addressesService, ParserService $transactionsService, RpcService $rpcService, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->addressesService = $addressesService;
        $this->rpcService = $rpcService;
        $this->transactionsService = $transactionsService;
    }




    public function actionParse()
    {

        $lastHeight = Transaction::find()->select('MAX(block) as block')->asArray()->all()[0]['block'] ?? 0;
        $lastHeight = (int)$lastHeight;
        if ($lastHeight > 0) {
            $lastHeight--;
        }


        while (true) {

            $newHeight = $this->rpcService->getHeight();



            for ($i = $lastHeight; $i <= $newHeight; $i++) {

                echo "\n";
                echo "\n";
                echo "Block: " . $i . '/' . $newHeight;

                $hash = $this->rpcService->getBlockHash($i);


                $block = $this->rpcService->getBlock($hash);
                $transactions = $block['tx'];


                $this->transactionsService->saveAllTransactions($transactions, $i, $block['time']);
            }


            $lastHeight = $newHeight + 1;
            sleep(60);
        }
    }


}