<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "vins".
 * @property int $id
 * @property int $transaction_id
 * @property int $number
 * @property int|null $address_id
 * @property Address $address
 * @property Transaction $transaction
 * @property Vout[] $vouts
 */
class Vin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vins';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transaction_id'], 'required'],
            [['transaction_id', 'address_id', 'number'], 'integer'],
            //            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            //            [['transaction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transaction::className(), 'targetAttribute' => ['transaction_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'transaction_id' => 'Transaction ID',
            'address_id'     => 'Address ID',
        ];
    }

    /**
     * Gets query for [[Address]].
     * @return ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * Gets query for [[Transaction]].
     * @return ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'transaction_id']);
    }

    /**
     * Gets query for [[Vouts]].
     * @return ActiveQuery
     */
    public function getVouts()
    {
        return $this->hasMany(Vout::className(), ['vin_id' => 'id']);
    }
}
