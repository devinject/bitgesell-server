<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "transactions".
 * @property int $id
 * @property int $time
 * @property string $txid
 * @property string $data
 * @property float $amount
 * @property int|null $block
 * @property Vin[] $vins
 * @property Vout[] $vouts
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['txid', 'data'], 'required'],
            [['data'], 'safe'],
            [['amount'], 'double'],
            [['block', 'time'], 'integer'],
            [['txid'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'    => 'ID',
            'txid'  => 'Txid',
            'data'  => 'Data',
            'block' => 'Block',
        ];
    }

    /**
     * Gets query for [[Vins]].
     * @return ActiveQuery
     */
    public function getVins()
    {
        return $this->hasMany(Vin::className(), ['transaction_id' => 'id']);
    }

    /**
     * Gets query for [[Vouts]].
     * @return ActiveQuery
     */
    public function getVouts()
    {
        return $this->hasMany(Vout::className(), ['transaction_id' => 'id']);
    }
}
