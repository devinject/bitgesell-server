<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "vouts".
 * @property int $id
 * @property int $transaction_id
 * @property int $number
 * @property int|null $address_id
 * @property float|null $amount
 * @property int|null $vin_id
 * @property Address $address
 * @property Transaction $transaction
 * @property Vin $vin
 */
class Vout extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vouts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transaction_id'], 'required'],
            [['transaction_id', 'address_id', 'number', 'vin_id'], 'integer'],
            [['amount'], 'number'],
            //            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
            //            [['transaction_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transaction::className(), 'targetAttribute' => ['transaction_id' => 'id']],
            //            [['vin_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vin::className(), 'targetAttribute' => ['vin_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'transaction_id' => 'Transaction ID',
            'address_id'     => 'Address ID',
            'amount'         => 'Amount',
            'vin_id'         => 'Vin ID',
        ];
    }

    /**
     * Gets query for [[Address]].
     * @return ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * Gets query for [[Transaction]].
     * @return ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasOne(Transaction::className(), ['id' => 'transaction_id']);
    }

    /**
     * Gets query for [[Vin]].
     * @return ActiveQuery
     */
    public function getVin()
    {
        return $this->hasOne(Vin::className(), ['id' => 'vin_id']);
    }
}
