<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "addresses".
 * @property int $id
 * @property string $address
 * @property Vin[] $vins
 * @property Vout[] $vouts
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'addresses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'required'],
            [['address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'      => 'ID',
            'address' => 'Address',
        ];
    }

    /**
     * Gets query for [[Vins]].
     * @return ActiveQuery
     */
    public function getVins()
    {
        return $this->hasMany(Vin::className(), ['address_id' => 'id']);
    }

    /**
     * Gets query for [[Vouts]].
     * @return ActiveQuery
     */
    public function getVouts()
    {
        return $this->hasMany(Vout::className(), ['address_id' => 'id']);
    }
}
