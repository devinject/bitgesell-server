<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transaction}}`.
 */
class m201115_191735_create_transaction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%transactions}}',
            [
                'id'     => $this->primaryKey(),
                'txid'   => $this->string()->notNull(),
                'data'   => $this->json()->notNull(),
                'block'  => $this->integer()->null(),
                'amount' => $this->float()->null(),
                'time'   => $this->integer()->null(),
            ]
        );

        $this->execute('create index transactions_txid_hash on transactions USING hash (txid);');
//        $this->createIndex('transactions-txid', 'transactions', 'txid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('transactions_txid_hash', 'transactions');
        $this->dropTable('{{%transaction}}');
    }
}
