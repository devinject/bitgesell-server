<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vins}}`.
 */
class m201116_212538_create_vins_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%vins}}',
            [
                'id'             => $this->primaryKey(),
                'transaction_id' => $this->integer()->notNull(),
                'number'         => $this->integer(),
                'address_id'     => $this->integer()->null(),
            ]
        );

//        $this->addForeignKey(
//            'fk-vins-transaction_id',
//            'vins',
//            'transaction_id',
//            'transactions',
//            'id'
//        );
//        $this->addForeignKey(
//            'fk-vins-address_id',
//            'vins',
//            'address_id',
//            'addresses',
//            'id'
//        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-vins-transaction_id',
            'vins'
        );
        $this->dropForeignKey(
            'fk-vins-address_id',
            'vins'
        );
        $this->dropTable('{{%vins}}');
    }
}
