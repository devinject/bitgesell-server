<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%address}}`.
 */
class m201115_191639_create_address_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%addresses}}',
            [
                'id'      => $this->primaryKey(),
                'address' => $this->string()->notNull()->unique(),
            ]
        );


        $this->execute('create index addresses_address_hash on addresses USING hash (address);');
//        $this->createIndex('addresses-address', 'addresses', 'address');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('addresses_address_hash', 'addresses');
        $this->dropTable('{{%address}}');
    }
}
