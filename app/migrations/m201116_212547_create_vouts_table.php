<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%vouts}}`.
 */
class m201116_212547_create_vouts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%vouts}}',
            [
                'id'             => $this->primaryKey(),
                'transaction_id' => $this->integer()->notNull(),
                'address_id'     => $this->integer()->null(),
                'amount'         => $this->float()->defaultValue(0),
                'number'         => $this->integer(),
                'vin_id'         => $this->integer()->null(),
            ]
        );

//        $this->addForeignKey(
//            'fk-vouts-transaction_id',
//            'vouts',
//            'transaction_id',
//            'transactions',
//            'id'
//        );
//        $this->addForeignKey(
//            'fk-vouts-address_id',
//            'vouts',
//            'address_id',
//            'addresses',
//            'id'
//        );
//        $this->addForeignKey(
//            'fk-vouts-vin_id',
//            'vouts',
//            'vin_id',
//            'vins',
//            'id'
//        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-vouts-transaction_id',
            'vouts'
        );
        $this->dropForeignKey(
            'fk-vouts-address_id',
            'vouts'
        );
        $this->dropForeignKey(
            'fk-vouts-vin_id',
            'vouts'
        );
        $this->dropTable('{{%vouts}}');
    }
}
