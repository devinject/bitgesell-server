<?php

namespace app\controllers;

use Yii;
use yii\base\Action;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;

use GuzzleHttp\Exception\GuzzleException;

use app\models\Transaction;
use app\models\Vout;

use app\services\DataService;
use app\services\ParserService;
use app\services\RpcService;

class SiteController extends Controller
{
    /**
     * @var DataService
     */
    private $addressesService;

    /**
     * @var RpcService
     */
    private $rpcService;

    /**
     * @var ParserService
     */
    private $parserService;

    /**
     * @param Action $action
     *
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (empty($_POST)) {
            $_POST                      = json_decode(file_get_contents('php://input'), true);
        }

        return parent::beforeAction($action);
    }

    /**
     * SiteController constructor.
     *
     * @param $id
     * @param $module
     * @param DataService $addressesService
     * @param RpcService $rpcService
     * @param ParserService $parserService
     * @param array $config
     */
    public function __construct(
        $id,
        $module,
        DataService $addressesService,
        RpcService $rpcService,
        ParserService $parserService,
        $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->addressesService = $addressesService;
        $this->rpcService       = $rpcService;
        $this->parserService    = $parserService;
    }

    /**
     * @return array
     * @throws GuzzleException
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_HTML;
        $this->layout = false;
        return $this->render('index');
    }


    /**
     * Displays homepage.
     *
     */
    public function actionAddresses()
    {
        $addresses = Yii::$app->request->post('addresses');
        return $this->addressesService->getTransactions($addresses);
    }

    /**
     * @return mixed
     * @throws GuzzleException
     */
    public function actionCreaterawtransaction()
    {
        $props = Yii::$app->request->post('props');
        if (gettype($props) === "string") {
            $props = json_decode($props, true);
        }
        $txhex = $this->rpcService->sendQuery('createrawtransaction', $props);

        return $txhex;
    }

    /**
     * @throws GuzzleException
     */
    public function actionSendrawtransaction()
    {
        $tx = $this->rpcService->sendQuery('decoderawtransaction', Yii::$app->request->post('props'));
        $this->rpcService->sendQuery('sendrawtransaction', Yii::$app->request->post('props'));
        $this->parserService->saveAllTransactions([$tx], null, time());
    }

    /**
     * @return array
     */
    public function actionGetMinInfo()
    {
        return [
            'minFee' => 100,
            'defaultFeeVbyte' => 2,
        ];
    }

    /**
     * Displays about page.
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
