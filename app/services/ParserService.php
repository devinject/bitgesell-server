<?php

namespace app\services;

use app\models\Address;
use app\models\Transaction;
use app\models\Vin;
use app\models\Vout;
use yii\helpers\ArrayHelper;

/**
 * Class ParserService
 * @package app\services
 */
class ParserService
{
    /**
     * @param $transactions
     * @param null $block
     * @param null $time
     */
    public function saveAllTransactions($transactions, $block = null, $time = null)
    {
        $runTime          = microtime(true);
        $vinsTransactions = $this->getVinsTransactions($transactions);
        $voutAddresses    = $this->getVoutAddresses($transactions);

        $runTime = microtime(true);

        foreach ($transactions as $transaction) {
            $this->saveTransaction($transaction, $block, $vinsTransactions, $voutAddresses, $time);
        }
    }

    /**
     * @param $apiTransactions
     *
     * @return array
     */
    public function getVinsTransactions($apiTransactions)
    {
        $insTransactions = [];

        $test = false;

        foreach ($apiTransactions as $transaction) {
            if (!isset($transaction['vin']) || !count($transaction['vin'])) {
                continue;
            }

            foreach ($transaction['vin'] as $vin) {
                if (!isset($vin['txid'])) {
                    continue;
                }

                if (!isset($insTransactions[$vin['txid']])) {
                    $insTransactions[$vin['txid']] = [];
                }

                $insTransactions[$vin['txid']][] = $vin['vout'];
            }
        }

        $txs = Transaction::find()
            ->where(['txid' => array_keys($insTransactions)])
            ->select(['id', 'txid'])->all();

        $txs = ArrayHelper::index($txs, 'txid');

        $vouts    = Vout::find()->where(['transaction_id' => ArrayHelper::getColumn($txs, 'id')])->all();
        $voutsNew = [];

        foreach ($vouts as $vout) {
            $voutsNew[$vout->transaction_id.'_'.$vout->number] = $vout;
        }

        unset($vouts);

        foreach ($insTransactions as $key => &$insTransaction) {
            $vins = [];

            foreach ($insTransaction as $number) {
                if (!isset($txs[$key])) {
                    continue;
                }

                if (!isset($voutsNew[$txs[$key]->id.'_'.$number])) {
                    continue;
                }

                $vins[$number] = $voutsNew[$txs[$key]->id.'_'.$number];
            }

            $insTransaction = $vins;
        }

        return $insTransactions;
    }

    /**
     * @param $apiTransactions
     *
     * @return array
     */
    public function getVoutAddresses($apiTransactions)
    {
        $addresses = [];

        foreach ($apiTransactions as $transaction) {
            if (!isset($transaction['vout']) || !count($transaction['vout'])) {
                continue;
            }

            foreach ($transaction['vout'] as $vout) {
                if (!$vout['value']) {
                    continue;
                }

                if (!isset($vout['scriptPubKey']) || !isset($vout['scriptPubKey']['addresses'])) {
                    continue;
                }

                if (!isset($vout['scriptPubKey']['addresses'][0])) {
                    continue;
                }

                $addresses[] = $vout['scriptPubKey']['addresses'][0];
            }
        }

        $voutAddresses = ArrayHelper::index(Address::find()->where(['address' => $addresses])->all(), 'address');

        foreach ($addresses as $address) {
            if (!isset($voutAddresses[$address])) {
                $newAddress          = new Address();
                $newAddress->address = $address;
                $newAddress->save();
                $voutAddresses[$address] = $newAddress;
            }
        }

        return $voutAddresses;
    }

    /**
     * @param $apiTransaction
     * @param null $blockId
     * @param null $vinsTransactions
     * @param null $voutAddresses
     * @param null $time
     */
    public function saveTransaction(
        $apiTransaction,
        $blockId = null,
        $vinsTransactions = null,
        $voutAddresses = null,
        $time = null
    ) {
        $oldTransaction = Transaction::find()
            ->where(['txid' => $apiTransaction['txid']])
            ->select(['id', 'txid', 'block'])
            ->one();

        if ($oldTransaction) {
            if ($oldTransaction->block) {
                return;
            }

            if ($blockId === null && $oldTransaction->block === null) {
                return;
            }

            if ($blockId) {
                $oldTransaction->block = $blockId;
                $oldTransaction->time  = $time;
                $oldTransaction->data  = $this->getTransactionData($apiTransaction);

                $oldTransaction->save();

                return;
            }
        }

        $transaction        = new Transaction();
        $transaction->txid  = $apiTransaction['txid'];
        $transaction->block = $blockId;
        $transaction->time  = $time;
        $transaction->data  = $this->getTransactionData($apiTransaction);

        $transaction->save();

        $voutsSum = $this->saveVouts($apiTransaction, $transaction->id, $voutAddresses, $vinsTransactions);
        $vinsSum  = $this->saveVins($apiTransaction, $transaction, $vinsTransactions);

        if ($voutsSum) {
            $transaction->amount = $voutsSum;
        }

        if ($vinsSum) {
            $transaction->amount = $vinsSum;
        }

        if ($vinsSum || $voutsSum) {
            $transaction->save();
        }
    }

    /**
     * @param $apiTransaction
     *
     * @return array
     */
    protected function getTransactionData($apiTransaction)
    {
        return [
            'vin'      => $apiTransaction['vin'],
            'vout'     => $apiTransaction['vout'],
            'weight'   => $apiTransaction['weight'],
            'version'  => $apiTransaction['version'],
            'locktime' => $apiTransaction['locktime'],
        ];
    }

    protected function saveVins($apiTransaction, Transaction $transaction, &$vinsTransactions)
    {
        if (!isset($apiTransaction['vin'])) {
            return;
        }

        $sum = 0;

        foreach ($apiTransaction['vin'] as $key => $value) {
            $vin                 = new Vin();
            $vin->transaction_id = $transaction->id;
            $vin->number         = $key;

            if (isset($value['txid'])) {
                if (isset($vinsTransactions[$value['txid']]) &&
                    isset($vinsTransactions[$value['txid']][$value['vout']])
                ) {
                    $sum             += $vinsTransactions[$value['txid']][$value['vout']]->amount;
                    $vin->address_id = $vinsTransactions[$value['txid']][$value['vout']]->address_id;
                }
            }

            $vin->save();

            if (isset($vinsTransactions[$value['txid']]) &&
                isset($vinsTransactions[$value['txid']][$value['vout']])
            ) {
                $vinsTransactions[$value['txid']][$value['vout']]->vin_id = $vin->id;
                $vinsTransactions[$value['txid']][$value['vout']]->save();
            }
        }

        return $sum;
    }

    /**
     * @param $apiTransaction
     * @param $id
     * @param $voutAddresses
     * @param $vinsTransactions
     *
     * @return float|int|mixed|void|null
     */
    protected function saveVouts($apiTransaction, $id, $voutAddresses, &$vinsTransactions)
    {
        if (!isset($apiTransaction['vout'])) {
            return;
        }

        $sum = 0;

        foreach ($apiTransaction['vout'] as $key => $value) {
            $vout                 = new Vout();
            $vout->transaction_id = $id;
            $vout->number         = $value['n'];

            if (!$value['value']) {
                continue;
            }

            $vout->amount = $value['value'];
            $sum          += $vout->amount;

            if (isset($value['scriptPubKey']) && isset($value['scriptPubKey']['addresses'])) {
                if (count($value['scriptPubKey']['addresses']) > 1) {
                    echo "\n";
                    echo "ALARM: ".$id;
                    die;
                }

                $addressName      = $value['scriptPubKey']['addresses'][0];
                $vout->address_id = $voutAddresses[$addressName]->id;
            }
            $vout->save();

            if (!isset($vinsTransactions[$apiTransaction['txid']])) {
                $vinsTransactions[$apiTransaction['txid']] = [];
            }

            $vinsTransactions[$apiTransaction['txid']][$vout->number] = $vout;
        }

        return $sum;
    }
}
