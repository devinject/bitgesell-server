<?php

namespace app\services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class RpcService
{
    /**
     * @param $height
     *
     * @return mixed
     * @throws GuzzleException
     */
    public function getBlockHash($height)
    {
        return $this->sendQuery('getblockhash', [$height]);
    }

    /**
     * @param $hash
     *
     * @return mixed
     * @throws GuzzleException
     */
    public function getBlock($hash)
    {
        return $this->sendQuery('getblock', [$hash, 2]);
    }

    /**
     * @return int
     * @throws GuzzleException
     */
    public function getHeight(): int
    {
        return $this->sendQuery('getblockcount');
    }

    /**
     * @param $method
     * @param array $params
     *
     * @return mixed
     * @throws GuzzleException
     */
    function sendQuery($method, $params = [])
    {
        $client   = new Client();
        $response = $client->request(
            'POST',
            'http://admin:secret@ubuntu:8455',
            [
                RequestOptions::JSON => [
                    "jsonrpc" => "1.0",
                    "id"      => "curltest",
                    "method"  => $method,
                    "params"  => $params,
                ],
            ]
        );

        return json_decode($response->getBody()->getContents(), true)['result'];
    }
}
