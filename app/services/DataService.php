<?php

namespace app\services;

use app\models\Address;
use app\models\Transaction;
use app\models\Vin;
use app\models\Vout;
use GuzzleHttp\Exception\GuzzleException;
use yii\helpers\ArrayHelper;

class DataService
{
    /**
     * @var RpcService
     */
    private $rpcService;

    /**
     * DataService constructor.
     *
     * @param RpcService $rpcService
     */
    public function __construct(RpcService $rpcService)
    {
        $this->rpcService = $rpcService;
    }

    /**
     * @param $addresses
     *
     * @return array
     * @throws GuzzleException
     */
    public function getTransactions($addresses)
    {
        $lastBlock = $this->rpcService->getHeight();

        // User addresses
        $addresses    = Address::find()->where(['address' => $addresses])->all();
        $addressesIds = ArrayHelper::getColumn($addresses, 'id');

        /**
         * Address related transactions
         * @var $transactions Transaction[]
         */
        $vins = Vin::find()->where(['address_id' => $addressesIds])->groupBy('transaction_id')->select(
            'transaction_id'
        )->asArray()->all();
        $vins = ArrayHelper::getColumn($vins, 'transaction_id');

        $vouts = Vout::find()->where(['address_id' => $addressesIds])->groupBy('transaction_id')->select(
            'transaction_id'
        )->asArray()->all();
        $vouts = ArrayHelper::getColumn($vouts, 'transaction_id');

        $transactions = Transaction::find()
            ->where(['transactions.id' => array_merge($vins, $vouts)])
            ->joinWith('vins', 'vouts')
            ->all();

        $addressesIds = [];

        foreach ($transactions as $transaction) {
            foreach ($transaction->vins as $vin) {
                if ($vin->address_id) {
                    $addressesIds[] = $vin->address_id;
                }
            }

            foreach ($transaction->vouts as $vout) {
                if ($vout->address_id) {
                    $addressesIds[] = $vout->address_id;
                }
            }
        }

        // All addresses in a transaction
        $allAddresses = ArrayHelper::index(Address::findAll(['id' => $addressesIds]), 'id');

        $userTransactions = [];

        foreach ($transactions as $transaction) {
            $from = [];

            foreach ($transaction->vins as $vin) {
                if ($vin->address_id && isset($allAddresses[$vin->address_id])) {
                    $from[] = $allAddresses[$vin->address_id]->address;
                }
            }

            $to = [];

            foreach ($transaction->vouts as $vout) {
                $script = null;

                if ($transaction->data['vout']) {
                    if ($transaction->data['vout'][$vout->number]) {
                        if ($transaction->data['vout'][$vout->number]['scriptPubKey']) {
                            $script = $transaction->data['vout'][$vout->number]['scriptPubKey'];
                        }
                    }
                }

                $to[] = [
                    'txid'         => $transaction->txid,
                    'scriptPubKey' => $script,
                    'address'      => $allAddresses[$vout->address_id]->address ?? null,
                    'amount'       => $vout->amount,
                    'number'       => $vout->number,
                    'used'         => $vout->vin_id ? true : false,
                ];
            }

            $userTransactions[] = [
                'transaction_id' => $transaction->txid,
                'amount'         => $transaction->amount,
                'date'           => $transaction->time,
                'confirmations'  => $transaction->block ? ($lastBlock - $transaction->block + 1) : 0,
                'from'           => $from,
                'to'             => $to,
            ];
        }

        return $userTransactions;
    }
}
