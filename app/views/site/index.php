<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BGL Wallet | Cross-platform wallet for Bitgesell</title>

    <link rel="shortcut icon" href="/images/256x256.png" type="image/png">
    <link rel="apple-touch-icon" href="/images/256x256.png">

    <link rel="stylesheet" type="text/css" href="/css/main.css?<?= time() ?>">
</head>
<body>
<div class="wrap">
    <div class="bg"></div>

    <div class="actions">
        <div class="more">
            <div class="icon">
                <svg width="12" height="3" viewBox="0 0 12 3" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                          d="M11 2.1499C10.4477 2.1499 10 1.70219 10 1.1499C10 0.597618 10.4477 0.149902 11 0.149902C11.5523 0.149902 12 0.597618 12 1.1499C12 1.70219 11.5523 2.1499 11 2.1499ZM6 2.1499C5.44772 2.1499 5 1.70219 5 1.1499C5 0.597617 5.44772 0.149902 6 0.149902C6.55228 0.149902 7 0.597618 7 1.1499C7 1.70219 6.55228 2.1499 6 2.1499ZM-4.37114e-08 1.1499C-6.78525e-08 1.70219 0.447716 2.1499 1 2.1499C1.55229 2.1499 2 1.70219 2 1.1499C2 0.597618 1.55229 0.149902 1 0.149902C0.447716 0.149902 -1.95703e-08 0.597618 -4.37114e-08 1.1499Z"
                          fill="white"/>
                </svg>
            </div>

            <div class="navs">
                <a href="https://bitgesell.ca/" target="_blank">Bitgesell</a>
                <a href="https://bitcointalk.org/index.php?topic=5238559" target="_blank">Forum</a>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="logo">
            <svg width="51" height="61" viewBox="0 0 51 61" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M35.9946 25.2267C38.3271 23.1294 39.3684 20.2572 37.8938 16.3324C35.8863 10.9667 30.6286 10.3959 24.4752 11.4809L22.4269 3.84096L17.7684 5.08919L19.7605 12.5289C18.5348 12.8573 17.2918 13.2161 16.0547 13.5743L14.0474 6.08731L9.39305 7.33398L11.4389 14.9726C10.4357 15.2634 9.45037 15.5484 8.48313 15.8076L8.47708 15.785L2.05085 17.5041L3.38242 22.4701C3.38242 22.4701 6.80529 21.4837 6.76542 21.5616C8.65198 21.0575 9.56092 21.9862 9.99101 22.8828L12.3247 31.5863L15.6006 43.8124C15.6763 44.4271 15.5805 45.4674 14.2621 45.8226C14.3362 45.8592 10.8732 46.73 10.8732 46.73L11.4372 52.5309L17.4985 50.9068C18.6283 50.6055 19.7427 50.3257 20.8337 50.0427L22.9081 57.7683L27.5616 56.5228L25.5116 48.8774C26.7988 48.5605 28.0375 48.2407 29.244 47.9157L31.2818 55.526L35.9401 54.2778L33.8758 46.5632C41.5859 44.016 46.54 40.5774 45.254 33.052C44.2193 26.993 40.7258 25.1008 35.9946 25.2267ZM17.6309 19.0115C20.2582 18.3075 28.2972 15.2581 29.7668 20.7381C31.1735 25.9925 22.7488 27.592 20.1188 28.2967L17.6309 19.0115V19.0115ZM24.116 43.2226L21.3748 32.9842C24.5313 32.1375 34.1894 28.5785 35.8047 34.6017C37.3539 40.3779 27.2741 42.3739 24.116 43.2226Z" fill="white"/>
            </svg>
        </div>

        <div class="title">BGL Wallet</div>

        <div class="text">
            <p>Welcome everyone! Here you can download the cross-platform wallet for Bitgesell.</p>
            <br>
            <p>It's safe. Your private keys are encrypted and stored locally.</p>
            <p>You can be sure in your funds and keys safety.</p>
            <p>If access to the wallet will be lost your funds can be recovered from a seed and passphrase.</p>
            <br>
            <p>Remember: «Bitgesell — Future of Bitcoin and True store of value»</p>
            <p>..and this application makes it easy to use!</p>
            <br>
            <p>The current stable version is 1.0.0</p>
        </div>

        <div class="line"></div>

        <div class="title small">Download</div>

        <div class="links">
            <div>
                <a href="/downloads/windows/BGL.Wallet.exe">
                    <span>Windows</span>
                </a>
            </div>

            <div>
                <span>Linux</span>

                <div class="navs">
                    <a href="/downloads/linux/BGL.Wallet-amd64.deb">amd64</a>
                    <a href="/downloads/linux/BGL.Wallet-i386.deb">i386</a>
                    <a href="/downloads/linux/BGL.Wallet-x86_64.AppImage">x86_64 AppImage</a>
                    <a href="/downloads/linux/BGL.Wallet-i386.AppImage">i386 AppImage</a>
                </div>
            </div>

            <div class="main">
                <a href="/downloads/macos/BGL.Wallet.dmg">
                    <span>macOS</span>
                </a>
            </div>
        </div>
    </div>
</div>

<script>
    function getOS() {
        let userAgent = window.navigator.userAgent,
            platform = window.navigator.platform,

            macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K', 'darwin'],
            windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'],
            iosPlatforms = ['iPhone', 'iPad', 'iPod'],

            os = null;

        if (macosPlatforms.indexOf(platform) !== -1) {
            os = 'Mac';
        } else if (iosPlatforms.indexOf(platform) !== -1) {
            os = 'iOS';
        } else if (windowsPlatforms.indexOf(platform) !== -1) {
            os = 'Windows';
        } else if (/Android/.test(userAgent)) {
            os = 'Android';
        } else if (!os && /Linux/.test(platform)) {
            os = 'Linux';
        }

        return os;
    }

    console.log(getOS());
</script>

</body>
</html>
